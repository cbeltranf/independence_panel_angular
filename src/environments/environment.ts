export const environment = {
  production: false,
  // apiUrl: 'http://190.131.235.19:8000/api/v1',
  apiUrl: 'https://apiindependence.inmov.info/api/v1',
  //assetsDomain: 'http://190.131.235.19:8000',
  assetsDomain: 'https://apiindependence.inmov.info',
  googleClientId: '900776205808-auim06fdsidl8m338smps614tod1s3h0.apps.googleusercontent.com',
  isMockEnabled: true // You have to switch this, when your real back-end is done
};